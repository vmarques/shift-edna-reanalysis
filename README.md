# Intro 

It is needed to have `ecotag` from the `obitools` toolkit installed.  
Here I do it with `conda` 
Also `R` and packages `tidyverse` and <...>

## Overview 

It is about   
1. Re-analyze processed projects with the teleo primers with both `swarm` and `obitools` pipeline with the most recent reference database (April 2023), a combination of the custom database with NCBI and ENA  
2. Once taxo assignment is re-launched, to process-clean them for use in biodiversity analysis 

## Usage 

Launch the master script `main.sh` 

The `ecotag_launch.sh` script will launch ecotag for all projects in need of a re-assignment for both swarm and obitools. 

The `clean_motus.R` script will process and clean swarm outputs while the `clean_obi.R` script will process and clean obitools outputs. 

Final outputs will be stored in `output/` folder 
