# ecotag_launch.sh 

# Here the script must take in input all files in swarm adn obitools folders to apply ecotag on it 

# SWARM outputs
for project in `ls data/to_relaunch_ecotag/swarm/`
do 

    # copy occurence table to the same directory
    #file_table=`ls data/to_relaunch_ecotag/swarm/${project}/*teleo_table*|grep -i ${project}_`
    #cp ${file_table} data/to_clean/swarm/${project}/.
    # copy metadata to the same directory
    #mkdir data/to_clean/swarm/${project}/metadata/
    #cp data/to_relaunch_ecotag/swarm/${project}/metadata/* data/to_clean/swarm/${project}/metadata/.

    file_table=`ls data/to_relaunch_ecotag/swarm/${project}/*teleo_table*|grep -i ${project}_`
    Rscript R/csv_to_fasta.R ${file_table} data/to_clean/swarm/${project}/temp/${project}_teleo.fasta 

    for files in `ls data/to_relaunch_ecotag/swarm/${project}/*teleo_ecotag_ncbiref*`
    do 
        # Exclude if not project
        if [[ "$file" =~ Other || "$file" =~ Blank ]]; then
              continue
        fi

        # process project
        #mkdir -p data/to_clean/swarm/${project}
        mkdir -p data/to_clean/swarm/${project}/temp

        #ecotag 
        ecotag -t data/ref_database/custom_taxonomy/ -R  data/ref_database/db_ncbi_and_custom.fasta -m 0 data/to_clean/swarm/${project}/temp/${project}_teleo.fasta > data/to_clean/swarm/${project}/temp/${project}_teleo_ecotag.fasta
        #obiannotate 
        obiannotate  --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
        --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
        --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
        --delete-tag=obiclean_head --delete-tag=obiclean_headcount \
        --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
        --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
        --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
        --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
        --delete-tag=tail_quality data/to_clean/swarm/${project}/temp/${project}_teleo_ecotag.fasta > data/to_clean/swarm/${project}/temp/${project}_teleo_annot.fasta

        # sort
        obisort -k count -r data/to_clean/swarm/${project}/temp/${project}_teleo_annot.fasta > data/to_clean/swarm/${project}/temp/${project}_teleo_annot_sort.fasta

        # table
        obitab -o data/to_clean/swarm/${project}/temp/${project}_teleo_annot_sort.fasta > data/to_clean/swarm/${project}/${project}_ecotag_teleo_done_2023.tsv

        # Now copy back to folder
        cp data/to_clean/swarm/${project}/${project}_ecotag_teleo_done_2023.tsv  data/to_relaunch_ecotag/swarm/${project}/

        # Remove temp folder now?

    done    
done

# OBITOOLS outputs 
for files in `ls data/to_relaunch_ecotag/obitools`
do 
    ecotag

done


for i in 0 1 2 3 4 5 6 7 8 9
do
    for j in 0 1 2 3 4 5 6 7 8 9
    do 
        echo "$i$j"
    done
done



ls data/to_relaunch_ecotag/swarm/${project}/*teleo_ecotag_ncbiref*

data/to_relaunch_ecotag/swarm/${project}/*teleo_ecotag_ncbiref*