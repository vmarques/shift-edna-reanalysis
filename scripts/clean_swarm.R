# This script reads eDNA metabarcoding data from the SWARM clustering pipeline 
# It cleans and formats data correctly 

# Lib 
library(tidyverse)
library(data.table)
library(conflicted)

conflict_prefer("select", "dplyr")
conflict_prefer("filter", "dplyr")

"%ni%" <- Negate("%in%")

# Source functions
source("R/clean_functions_swarm.R")
load("data/archive_class_ncbi.Rdata")

# List the directories 
list_projects_dir <- list.dirs(path = "data/to_relaunch_ecotag/swarm", full.names = TRUE, recursive = F)

# Create outputs list
list_read_step1 <- list()
list_clean_lot_discarded <- list()

directories_multiples_projects <- c("malpelo", "fakarava", "santamarta", "providencia")

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 
# Step 1 - Assemble & clean
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 

# Clean the index-hoping (ie. same plate number, different library)
# Clean the tag-jump (1/1000 threshold for each MOTU within each library)

for(i in 1:length(list_projects_dir)){
  
  # i = 2
  dir_i <- list_projects_dir[[i]]
  project_i <- word(dir_i, sep="/", 4)
  
  # Open files --- DO A WARNING HERE IF 0 FILES 
  files_i <- list.files(path=dir_i, pattern = "(.*)teleo(.*)", recursive = F, include.dirs = FALSE)
  
  if(length(files_i)==0){next}
  
  # Metadata file 
  metadata_i <- fread(paste0(dir_i, "/metadata/all_samples.csv"), sep=";", h=F, stringsAsFactors = F) # %>% # There is sometimes a bug where '-' in original metadata ends up being "." after processing - correct it here -- it only happends with read.csv!! (weird) not fread mutate(V3 = gsub("\\-","\\.",V3))
  colnames(metadata_i)[1:5] <- c("plaque", "run", "sample_name", "project", "marker")
  
  # Check if the lot column is present or not 
  if(dim(metadata_i)[2] == 6){
    colnames(metadata_i)[6] <- c("lot")
    }
  
  # ----- # Open files 
  
  # For the project 
  project_table <- fread(
    paste0(dir_i, "/", grep(paste0(
      project_i, "(.*)table"), files_i, value=T, ignore.case = TRUE)),
    sep="\t", stringsAsFactors = F, h=T)
  
  # Modif here to take only the double combination of ncbi and custom database
  project_taxo <- fread(
    paste0(dir_i, "/", grep(paste0(
      project_i, "(.*)ecotag_teleo_done_2023"), files_i, value=T, ignore.case = TRUE)),
    sep="\t", stringsAsFactors = F, h=T)
  
  # -------- # For the other files
  # If there are multiple projects within a directory, the other projects needs to be grouped with the 'Other' files. 
  # Ifelse condition here to filter 
  # TEST ICI: REMOVE THE BLANKS TO BE ADDED: IT IS NOT OTHER FILES 
  
  # CONDITION 1: Other et pas de multiple
  # CONDITION 2: Other et multiple 
  # CONDITION 3: Pas de Other
  # CONDITION 4: Pas de other et multiple
  
  # project_i %in% directories_multiples_projects
  # length(grep("Other(.*)", files_i, value=T)) == 0 # There is no others files
  # length(grep("Other(.*)", files_i, value=T)) > 0 # There is others files
  
  if(project_i %ni% directories_multiples_projects & length(grep("Other(.*)", files_i, value=T)) > 0){
    {
      ##### IF THERE IS A SINGLE PROJECT WITHIN THE DIRECTORY (CLASSIC CASE) & THERE IS OTHERS FILES 
      
      # Other 
      other_table <- fread(paste0(dir_i, "/", grep("Other(.*)table", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)
      other_taxo <- fread(paste0(dir_i, "/", grep("Other(.*)ecotag_customref", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T) # Modif here to take only the double combination of ncbi and custom database
      
      # Assemble
      other_data <- assemble_data(table_otu = other_table, taxo_otu = other_taxo) %>%
        left_join(., metadata_i)
      
    }
  } else if (project_i %in% directories_multiples_projects & length(grep("Other(.*)", files_i, value=T)) > 0){
    ##### IF THERE IS A MULTIPLE PROJECTS WITHIN THE DIRECTORY (CLASSIC CASE) & THERE IS OTHERS FILES 
    message(paste0("There is multiple projects within the ", project_i, " directory, so other projects will be condensed together"))
    
    # Other - normal
    other_table <- fread(paste0(dir_i, "/", grep("Other(.*)table", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)
    other_taxo <- fread(paste0(dir_i, "/", grep("Other(.*)ecotag_customref", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)# Modif here to take only the double combination of ncbi and custom database
    other_data_part1 <- assemble_data(table_otu = other_table, taxo_otu = other_taxo) %>%
      left_join(., metadata_i)
    
    # Other -- the other(s) project(s) also analyzed 
    other_projects <- unique(word(grep(paste0(project_i, "|Other|Blank"), files_i, ignore.case = T, value=T, invert=T), 1, sep="_"))
    
    list_other <- lapply(other_projects, function(x){
      
      # x = "Fakarava"
      # Same thing
      other_bis_table <- fread(paste0(dir_i, "/", grep(paste0(x, "(.*)table"), files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)
      other_bis_taxo <- fread(paste0(dir_i, "/", grep(paste0(x, "(.*)ecotag_customref"), files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)# Modif here to take only the double combination of ncbi and custom database
      other_bis_data <- assemble_data(table_otu = other_bis_table, taxo_otu = other_bis_taxo) %>%
        left_join(., metadata_i) %>%
        mutate(project = "Other")
    })
    
    # Bind 
    other_data_part2 <- bind_rows(list_other)
    
    # Bind all other projects together
    other_data <- rbind(other_data_part1, other_data_part2)
  } else if (project_i %ni% directories_multiples_projects & length(grep("Other(.*)", files_i, value=T)) == 0){
    ##### IF THERE IS A SINGLE PROJECT WITHIN THE DIRECTORY (CLASSIC CASE) & THERE IS NO OTHERS FILES 
    
    other_data <- data.frame()
    
  } else if (project_i %in% directories_multiples_projects & length(grep("Other(.*)", files_i, value=T)) == 0){
    ##### IF THERE IS A MULTIPLE PROJECTS WITHIN THE DIRECTORY (CLASSIC CASE) & THERE IS NO OTHERS FILES 
    
    # Other -- the other(s) project(s) also analyzed 
    other_projects <- unique(word(grep(paste0(project_i, "|Other|Blank"), files_i, ignore.case = T, value=T, invert=T), 1, sep="_"))
    
    list_other <- lapply(other_projects, function(x){
      
      # x = "Fakarava"
      # Same thing
      other_bis_table <- fread(paste0(dir_i, "/", grep(paste0(x, "(.*)table"), files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)
      other_bis_taxo <- fread(paste0(dir_i, "/", grep(paste0(x, "(.*)ecotag_customref"), files_i, value=T)), sep="\t", stringsAsFactors = F, h=T)
      other_bis_data <- assemble_data(table_otu = other_bis_table, taxo_otu = other_bis_taxo) %>%
        left_join(., metadata_i) %>%
        mutate(project = "Other")
    })
    
    # Bind 
    other_data <- bind_rows(list_other)
  }
  
  # ----- # Assemble project data 
  project_data <- assemble_data(table_otu = project_table, taxo_otu = project_taxo) %>%
    left_join(., metadata_i)
  
  # Add a control message for samples with SPY which do not match metadata (other than other project)
  
  # ----- # Blanks 
  # Blanks - not always present, and sometimes present but represent empty files
  # Apply the code using the blanks only if those are present and not empty

  # Find empty files
  files_blank0 <- system(paste0("find ", dir_i, " -type f -size 0 -print"), intern = TRUE)
  
  # No files = no blanks
  if(length(grep("Blank(.*)", files_i, value=T)) == 0){
    project_data_clean <- project_data
    message(paste0("There is no blank files for the ", project_i, " data"))
  }
  
  # Files present but the Blank_teleo has an empty file, then print it is empty 
  if(length(grep("Blank(.*)", files_i, value=T)) != 0  & sum(grepl("Blank_teleo", files_blank0), na.rm = TRUE) > 0){
    project_data_clean <- project_data
    message(paste0("Blank files are empty for the ", project_i, " data"))
  }
  
  # Apply this only if blank files exist and if those are not empty
  if(length(grep("Blank(.*)", files_i, value=T)) != 0 & sum(grepl("Blank_teleo", files_blank0), na.rm = TRUE) == 0){
    
    # Read 
    blank_table <- try(fread(paste0(dir_i, "/", grep("Blank(.*)table", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T))
    blank_taxo <- try(fread(paste0(dir_i, "/", grep("Blank(.*)ecotag_custom", files_i, value=T)), sep="\t", stringsAsFactors = F, h=T))
    
    # Assemble
    blank_data <- assemble_data(table_otu = blank_table, taxo_otu = blank_taxo) %>%
      left_join(., metadata_i)
    
    # CHeck NAs 
    lesna <- blank_data %>% filter(is.na(plaque))
    
    # Clean index-hoping (inter-library tag-jump)
    project_data_clean <- clean_index_hoping(file_edna = rbind(project_data, other_data), 
                                       file_blank = blank_data)[[1]]
    project_data_blanks_discarded <- clean_index_hoping(file_edna = rbind(project_data, other_data), 
                                                        file_blank = blank_data)[[2]]
    
    seuil <- clean_index_hoping(file_edna = rbind(project_data, other_data), 
                                file_blank = blank_data)[[3]]
    
    message(paste0("The Blank threhsold for the ", project_i, " project is ", seuil$seuil_blank))
    
  } else { 
    project_data_blanks_discarded <- data.frame()
    project_data_clean <- project_data
    }
  
  # Verifs - no NA values on the run
  verif_metadata <- !is.na(project_data_clean$run)
  if( length(verif_metadata[verif_metadata==FALSE]) > 0 ) stop(paste(" error: some samples do not have metadata fields"))
  
  # Verify NAs
  les_na <- project_data_clean %>% filter(is.na(run))
  
  # Clean tag-jump (output is a list, using [[2]] will output the discarded reads)
  project_data_tag <- clean_tag_jump(file_edna = project_data_clean, file_other = other_data)[[1]]
  
  # Add project column - normally not necessary as present in metadata but just in case 
  project_data_tag$project_i <- project_i
  
  # Store in list 
  list_read_step1[[i]] <- project_data_tag
  list_clean_lot_discarded[[i]] <- project_data_blanks_discarded
  
  # Print
  print(paste0(i, "_",  project_i))
  
}

# save(list_read_step1, list_clean_lot_discarded, file = "Rdata/all_df_step1.Rdata")
# load("Rdata/all_df_step1.Rdata")

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 
# Step 2: Clean and complete taxonomy
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 

load("Rdata/archive_class_ncbi.Rdata")

# Remove null elements from list
list_read_step1 <- list_read_step1[!sapply(list_read_step1,is.null)]

# Create and/or update archive file 
for(i in 1:length(list_read_step1)){
  
  file <- list_read_step1[[i]]
  
  # Filter out null object
  if(is.null(file)){next}
  
  # Clean taxonomy 
  file_taxo <- clean_taxonomy(file)
  
  # Add class name column 
  list_output <- add_class_name_archive(file_taxo, archive_class_ncbi)
  archive_class_ncbi <- list_output[[2]]
}

# Apply workflow
list_read_step2 <- lapply(list_read_step1, function(file){
  
  # Filter out null object
  if(is.null(file)){next}
  
  # Clean column names 
  columns_to_remove <- c("amplicon", "family", "genus", "order", "species", "taxid", "OTU", "total", 
                         "cloud", "length", "abundance", "spread", "identity", "taxonomy", "references")
  
  file_short <- file %>%
    select(-one_of(columns_to_remove))
  
  # Clean taxonomy 
  file_taxo <- clean_taxonomy(file_short)
  
  # Add class name column 
  list_output <- add_class_name_archive(file_taxo, archive_class_ncbi)
  file_taxo_all <- list_output[[1]]
  archive_class_ncbi <- list_output[[2]]
  
  # output
  return(file_taxo_all)
})

# Save the new archive file 
save(archive_class_ncbi, file = "data/archive_class_ncbi.Rdata")

# Save data
save(list_read_step1, list_clean_lot_discarded, list_read_step2, file = "data/temp/01_read_data.Rdata")

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 
# Step 3 - data cleaning 
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 

# Faire un bout de manual cleaning pour enlever les trucs bizarre de freshwater 


# Detail technique: une seule PCR sur la totalité du jeu de données, ou par projet (comme c'est le cas en ce moment) ? --> changer vers 1 PCR sur tout le jeu de données et voir l'impact sur la richesse??
# Clean with default values - so minimum 2 PCR replicates
list_read_step3 <- lapply(list_read_step2, function(x){
  clean_motus(x, min_PCR = 1)[[1]]
})

# here is a solution to apply the pcr filter over all projects and not just by project (it makes less sense)
list_read_step3bis_temp <- lapply(list_read_step2, function(x){
  clean_motus(x, min_PCR = 0)[[1]]
})

list_read_step3bis <- bind_rows(list_read_step3bis_temp) %>% 
    # count nb of pcr rep 
    group_by(sequence) %>%
    mutate(n_PCR = n_distinct(sample_name)) %>%
    ungroup() %>% 
    # filter
    filter(n_PCR >= 2) %>% 
    # put back into a list 
    group_split(project)

# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 
# Step 4 - LULU post-clustering
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- # 

# Set the path 
path_lulu <- "data/LULU/"

# To apply this function, you need to have a UNIX OS system (The system function doesn't work on windows I think)
# And you need to install the blastn tools in your local machine 

list_read_step4 <- lapply(list_read_step3, function(x){
  # Apply LULU
  list_LULU <- apply_lulu(x, path_lulu)
  # MOTUs to keep
  lulu_motus_keep <- list_LULU$curated_otus
  # Filter out discarded MOTUs
  x_filtered <- x %>%
    filter(definition %in% lulu_motus_keep)
  # End 
  return(x_filtered)
})

# save data 
save(list_read_step4, file="outputs/list_swarm.Rdata")

# Save a df per project or put everything together
all_df_swarm <- bind_rows(list_read_step4)

write.csv(all_df_swarm, "outputs/swarm/all_swarm_clean.csv", row.names = F)

# by project 
lapply(list_read_step4, function(x){
    project <- unique(x$project)
    write.csv(x, paste0("outputs/swarm/", project, "_swarm_clean.csv"), row.names=F, quote=F)
})